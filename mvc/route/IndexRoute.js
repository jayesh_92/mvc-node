import {
    Test as test
} from './../controller';
import express from 'express';
var router = express.Router();

router.route('/') 
    .get(function(req, res, next) {
    res.status(200);
    res.send({
        status: 'running',
        message: 'Welcome to test api'
    });
});


router.route('/test')
.get( function(req, res, next) {
    res.send(test.test())
});


module.exports = router;