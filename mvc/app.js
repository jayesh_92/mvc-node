var express = require('express');

var path = require('path');
var cookieParser =require('cookie-parser');
var bodyParser =require('body-parser');


var app = express();
var debug = require('debug')('base-arch:server');
var http = require('http');

function normalizePort(val) {
var port = parseInt(val, 10);

if (isNaN(port)) {
    // named pipe
    return val;
}

if (port >= 0) {
    // port number
    return port;
}

return false;
}


var port = normalizePort(process.env.PORT || '3001');
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port, () => console.log('Server is Listening on ' + port));

//parse application/json
app.use(bodyParser.json());

//parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
	extended: false
}))
app.use(cookieParser());

var index = require('./route/index')(app);

app.use(index);
app.use(express.static(path.join(__dirname,'public')));
app.set('views',path.join(__dirname,'public'));
app.engine('html',require('ejs').renderFile);
app.set('view engine','html');




